#include <iostream>
#undef min
#undef max

namespace GOTHIC_NAMESPACE {
  HBINK g_HBinkVideo = nullptr;


  bool IsSubtitlesEnabled() {
    return
      screen &&
      parser &&
      gameMan &&
      gameMan->videoPlayer &&
      gameMan->videoPlayer->IsPlaying();
  }


  unsigned long GetFrameTime() {
    static BINKREALTIME binkRealtime;
    BinkGetRealtime( g_HBinkVideo, &binkRealtime, 0 );
    unsigned long realFrameRate = g_HBinkVideo->FrameRate / g_HBinkVideo->FrameRateDiv;
    return binkRealtime.CurrentFrameID * realFrameRate;
  }


  zSTRING GetVideoName() {
    Union::String fileName = gameMan->videoPlayer->mVideoFilename.ToChar();
    Union::String videoName = fileName.GetFileNameNoExtension();
    return videoName.ToChar();
  }


  void PrintSubtitlesDebug() {
    if( zoptions->ReadBool( "Bink",  "BinkDebugInfo", 0 ) ) {
      screen->PrintCX( 100, GetVideoName() );
      screen->PrintCX( 500, zSTRING( GetFrameTime() ) + "ms" );
    }
  }


  void PrintSubtitles() {
    if( !IsSubtitlesEnabled() )
      return;

    PrintSubtitlesDebug();

    int funcIndex = parser->GetIndex( "G_BinkPrintSubtitles_" + GetVideoName() );
    if( funcIndex == -1 )
      return;

    static zCPar_Symbol* BINK_NPCNAME = parser->GetSymbol( "BINK_NPCNAME" );
    static zCPar_Symbol* BINK_MESSAGE = parser->GetSymbol( "BINK_MESSAGE" );
    if( !BINK_NPCNAME || !BINK_MESSAGE )
      return;

    for( int i = 0; i < BINK_NPCNAME->ele; i++ )
      BINK_NPCNAME->stringdata[i] = "";

    for( int i = 0; i < BINK_MESSAGE->ele; i++ )
      BINK_MESSAGE->stringdata[i] = "";
    
    parser->CallFunc( funcIndex, GetFrameTime() );

    int lineCount = std::max( BINK_NPCNAME->ele, BINK_MESSAGE->ele );
    for( int i = 0; i < lineCount; i++ ) {
      zSTRING npcName;
      zSTRING message;

      if( i < BINK_NPCNAME->ele ) {
        npcName = BINK_NPCNAME->stringdata[i];
        if( !npcName.IsEmpty() )
          npcName.Append( ": " );
      }

      if( i < BINK_MESSAGE->ele )
        message = BINK_MESSAGE->stringdata[i];


      static zCView* printView = []() {
        zCView* view = new zCView( 0, 0, 8192, 8192 );
        zSTRING fontName = "FONT_OLD_10_WHITE.TGA";
        zCPar_Symbol* symbol = parser->GetSymbol( "BINK_SUBTITLES_FONT_NAME" );
        if( symbol ) fontName = symbol->stringdata[0];
        view->SetFont( fontName );
        return view;
      }();

      screen->InsertItem( printView );
      {
        int npcNameWidth   = printView->FontSize( npcName );
        int messageWidth   = printView->FontSize( message );
        int fontHeight     = printView->FontY();
        int printPositionX = 4196 - (npcNameWidth + messageWidth) / 2;
        int printPositionY = 6500 + i * (fontHeight + 100);

        printView->ClrPrintwin();
        printView->SetFontColor( GFX_YELLOW );
        printView->Print( printPositionX, printPositionY, npcName );
        printPositionX += npcNameWidth;
        printView->SetFontColor( GFX_WHITE );
        printView->Print( printPositionX, printPositionY, message );
        printView->Blit();
      }
      screen->RemoveItem( printView );
    }
  }


#if ENGINE == Engine_G2A
  void* Partial_zCBinkPlayer_PlayFrame_Hookpoint = (void*)0x00657688;
#else // ENGINE == Engine_G1
  void* Partial_zCBinkPlayer_PlayFrame_Hookpoint = (void*)0x0071EDB8;
#endif
  auto Partial_zCBinkPlayer_PlayFrame = Union::CreatePartialHook( Partial_zCBinkPlayer_PlayFrame_Hookpoint, []( Union::Registers& reg ) {
    PrintSubtitles();
  } );


  HBINK __stdcall Hook_BinkOpen_Proc( const char* name, unsigned long flags );
  auto Hook_BinkOpen = Union::CreateHook( GetProcAddress( LoadLibrary( "BinkW32.dll" ), "_BinkOpen@8"), &Hook_BinkOpen_Proc, Union::HookType::Hook_Detours);

  HBINK __stdcall Hook_BinkOpen_Proc( const char* name, unsigned long flags ) {
    g_HBinkVideo = Hook_BinkOpen( name, flags );
    return g_HBinkVideo;
  }
}