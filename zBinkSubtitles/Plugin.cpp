#include <Union/Hook.h>
#include <Union/HookPartial.h>
#include <ZenGin/zGothicAPI.h>
#include "BinkAPI.h"

#ifdef __G1
#define GOTHIC_NAMESPACE Gothic_I_Classic
#define ENGINE Engine_G1
HOOKSPACE( Gothic_I_Classic, GetGameVersion() == ENGINE );
#include "Plugin.hpp"
// Your headers here . . .
#endif

#ifdef __G1A
#define GOTHIC_NAMESPACE Gothic_I_Addon
#define ENGINE Engine_G1A
HOOKSPACE( Gothic_I_Addon, GetGameVersion() == ENGINE );
#include "Plugin.hpp"
// Your headers here . . .
#endif

#ifdef __G2
#define GOTHIC_NAMESPACE Gothic_II_Classic
#define ENGINE Engine_G2
HOOKSPACE( Gothic_II_Classic, GetGameVersion() == ENGINE );
#include "Plugin.hpp"
// Your headers here . . .
#endif

#ifdef __G2A
#define GOTHIC_NAMESPACE Gothic_II_Addon
#define ENGINE Engine_G2A
HOOKSPACE( Gothic_II_Addon, GetGameVersion() == ENGINE );
#include "Plugin.hpp"
// Your headers here . . .
#endif

HOOKSPACE( Global, true );
