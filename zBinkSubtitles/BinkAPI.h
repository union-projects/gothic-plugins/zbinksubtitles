#pragma once
#include <Windows.h>


typedef struct BINKREALTIME {
  unsigned long CurrentFrameID;
  unsigned long FrameRate;
  unsigned long FrameRateDiv;
private:
  unsigned long Values[11];
} *HBINKREALTIME;


typedef struct BINK {
private:
  unsigned long Values[5];
public:
  unsigned long FrameRate;
  unsigned long FrameRateDiv;
} *HBINK;


typedef void( __stdcall* LPBinkGetRealtimeProc )(HBINK, HBINKREALTIME, unsigned long);


void __stdcall BinkGetRealtime( HBINK hBink, HBINKREALTIME hBinkRealtime, unsigned long a0 ) {
  static LPBinkGetRealtimeProc subProcess = []() {
    HMODULE module = LoadLibrary( "BinkW32.dll" );
    if( module == nullptr ) {
      Union::StringANSI( "BinkW32 not found!" ).ShowMessage();
      terminate();
    }

    void* proc = GetProcAddress( module, "_BinkGetRealtime@12" );
    if( proc == nullptr ) {
      Union::StringANSI( "_BinkGetRealtime@12 not found!" ).ShowMessage();
      terminate();
    }

    return (LPBinkGetRealtimeProc)proc;
  }();

  subProcess( hBink, hBinkRealtime, a0 );
}