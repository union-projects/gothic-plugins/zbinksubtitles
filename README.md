# zBinkSubtitles

This plugin can print the text on top of the game video.
To print text, you should create a `.d` file and implement the next definitions:
1. String arrays: `Bink_NpcName[]` and `Bink_Message[]`. Each array item will correspond to the text line. `Bink_NpcName` item specifies when the line should starts with actor name. You can skip this option if the line have no actor or it is a continuation of the previous line.
2. Optionaly can be defined a `BINK_SUBTITLES_FONT_NAME` string constant to specify a user font for the subtitles.
3. For any video, that should be subtitled, must be defined function with the next signature `func void G_BinkPrintSubtitles_<VIDEO_NAME>(var int frameTime)`, where `frameTime` is an elapsed video time in milliseconds.
4. In the first time will be defined a new option in `Gothic.ini` to show the current video name and frame time: `[BINK] BinkDebugInfo`.

Example script for the `INTRO.bik`:
```c
var string Bink_NpcName[5];
var string Bink_Message[5];

func void G_BinkPrintSubtitles_INTRO(var int frameTime) {
    if( frameTime >= 0 && frameTime <= 5000 ) {
        Bink_NpcName[0] = "Me";
        Bink_Message[0] = "Hello there";
    };
    if( frameTime >= 2500 && frameTime <= 7000 ) {
        Bink_NpcName[1] = "You";
        Bink_Message[1] = "It's me, Marrrio";
    };
    if( frameTime >= 5000 && frameTime <= 9000 ) {
        Bink_NpcName[0] = "Xardas";
        Bink_Message[0] = "Do you want some magic?";
    };
};
```

# Installation
Download the latest release [here](https://gitlab.com/union-projects/gothic-plugins/zBinkSubtitles/-/releases)
- For the Union-based game - place `zBinkSubtitles.vdf` to the `Gothic/Data/plugins`.
- For the SystemPack-based game - place `zBinkSubtitles.dll` and `UnionAPI.dll` to the `Gothic/System` and specify this libraries in the `pre.load` file.
